﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Car.Library
{
    class CarInfo 
    {
        //свойства
        private string _Model="";
        private string _Color="";        
        private int _CarMaxSpeed = 230;
        private string _CarPassability = "";
        private string _CarTypeDrive = "";
        private int _CarNumberSeats = 0;

        public string Model
        {
            get 
            {
                return _Model;
            }
            set 
            {
                _Model = value;
            }
        }
            public string Color
        {
            get 
            {
                return _Color;
            }
            set 
            {
                _Color = value;
            }
         }
        
        public int CarMaxSpeed
        {
            get 
            {
                return _CarMaxSpeed;
            }
            set 
            {
                _CarMaxSpeed = value;
            }
         }
        public string  CarPassability
        {
            get 
            {
                return _CarPassability;
            }
            set 
            {
                _CarPassability = value;
            }
         }
        public string  CarTypeDrive
        {
            get 
            {
                return _CarTypeDrive;
            }
            set 
            {
                _CarTypeDrive = value;
            }
         }
        public int  CarNumberSeats
        {
            get
            {
                return _CarNumberSeats;
            }
            set
            {
                _CarNumberSeats = value;
            }
        }
    }
     }


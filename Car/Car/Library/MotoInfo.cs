﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Car.Library
{
    class MotoInfo : CarInfo
    {
        private string _Lyulka = "";

        public string Lyulka
        {
            get
            {
                return _Lyulka;
            }
            set
            {
                _Lyulka = value;
            }
        }
        public int CarNumberSeats
        {
            get
            {
                return base.CarNumberSeats;
            }
            set
            {
                if (value <= 4) 
                { 
                    base.CarNumberSeats = value; 
                }
            }
           
        }
    }
}
